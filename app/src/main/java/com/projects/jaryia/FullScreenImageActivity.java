package com.projects.jaryia;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;
import com.projects.jaryia.service.OnSwipeTouchListener;

public class FullScreenImageActivity extends AppCompatActivity {
    private PhotoView photoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_image);
        String imageUrl = getIntent().getStringExtra("imageUrl");

        // Load the image into a PhotoView
        photoView = findViewById(R.id.photo_view);
        Glide.with(getApplicationContext())
                .load(imageUrl)
                .placeholder(R.drawable.logo)
                .error(R.drawable.twitter)
                .into(photoView);

        photoView.setOnTouchListener(new OnSwipeTouchListener(this) {
            public void onSwipeTop() {
                onBackPressed();
            }

            public void onSwipeRight() {
                onBackPressed();
            }

            public void onSwipeLeft() {
                onBackPressed();
            }

            public void onSwipeBottom() {
                onBackPressed();
            }

        });

    }

}