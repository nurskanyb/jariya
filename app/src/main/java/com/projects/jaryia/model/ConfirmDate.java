package com.projects.jaryia.model;

public class ConfirmDate {
    private String author, description, number, type, imageLinks, date;

    private boolean checker = false;

    public ConfirmDate(String author, String description, String number, String type, String imageLinks, String date) {
        this.author = author;
        this.description = description;
        this.number = number;
        this.type = type;
        this.imageLinks = imageLinks;
        this.date = date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getImageLinks() {
        return imageLinks;
    }

    public void setImageLinks(String imageLinks) {
        this.imageLinks = imageLinks;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean getChecker() {
        return checker;
    }

    public void setChecker(boolean checker) {
        this.checker = checker;
    }
}
