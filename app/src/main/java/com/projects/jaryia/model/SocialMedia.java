package com.projects.jaryia.model;

public class SocialMedia {
    private String number;

    public SocialMedia(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
