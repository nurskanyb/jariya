package com.projects.jaryia;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.projects.jaryia.model.ConfirmDate;
import com.projects.jaryia.service.CloudinarySave;
import com.projects.jaryia.service.DialogPages;
import com.projects.jaryia.service.FirebaseInfo;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class AddAD extends AppCompatActivity implements View.OnClickListener {
    private EditText author, description, number;
    private Button inputImage, saveAll;
    private Spinner type;
    private ArrayList<Bitmap> imageLinks = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_ad);
        init();
    }

    private void init() {
        author = findViewById(R.id.author);
        description = findViewById(R.id.description);
        number = findViewById(R.id.numbers);

        type = findViewById(R.id.type);

        inputImage = findViewById(R.id.selectImage);
        saveAll = findViewById(R.id.saveAd);

        inputImage.setOnClickListener(this);
        saveAll.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.selectImage:
                selectImages();
                break;
            case R.id.saveAd:
                saveAllInfo();
                break;
        }
    }

    private void saveAllInfo() {
        if (author.getText().toString().equals("") || description.getText().toString().equals("")|| number.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Баардык талааны толтурунуз", Toast.LENGTH_SHORT).show();
        } else {
            CloudinarySave cloudinarySave = new CloudinarySave(imageLinks);
            FutureTask<String> task = new FutureTask<>(cloudinarySave);
            Thread uploadImage = new Thread(task);
            String images = "";
            uploadImage.start();

            try {
                images = task.get();
            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }

            FirebaseInfo info = new FirebaseInfo(new ConfirmDate(author.getText().toString(), description.getText().toString(),
                    number.getText().toString(), type.getSelectedItem().toString(), images, currentDate()));
            info.addDatatoFirebase(this);

        }
    }

    private String currentDate() {
        LocalDate currentDate;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            currentDate = LocalDate.now();
            int day = currentDate.getDayOfMonth();
            int month = currentDate.getMonthValue();
            int year = currentDate.getYear();
            return day + "/" + month + "/" + year;
        } else
            return "date";
    }

    private void selectImages() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK && null != data) {
            if (data.getClipData() != null) {
                int count = data.getClipData().getItemCount();
                for (int i = 0; i < count; i++) {
                    try {
                        Uri imageUrl = data.getClipData().getItemAt(i).getUri();
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUrl);
                        imageLinks.add(bitmap);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            } else {
                try {
                    Uri imageUrl = data.getData();
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUrl);
                    imageLinks.add(bitmap);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } else {
            Toast.makeText(this, "You haven't picked Image", Toast.LENGTH_LONG).show();
        }
    }
}