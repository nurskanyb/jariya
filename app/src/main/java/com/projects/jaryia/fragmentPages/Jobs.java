package com.projects.jaryia.fragmentPages;

import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.projects.jaryia.R;
import com.projects.jaryia.model.News;
import com.projects.jaryia.service.RecyclerSort;

import java.util.ArrayList;

public class Jobs extends Fragment {

    private final ArrayList<News> allInformation = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_all, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SwipeRefreshLayout swipeRefreshLayout = view.findViewById(R.id.refreshLayout);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            Toast.makeText(getContext(), "Жаныланды", Toast.LENGTH_SHORT).show();
            requireActivity().finish();
            startActivity(requireActivity().getIntent());
            swipeRefreshLayout.setRefreshing(false);
        });

        TextView jariya;
        jariya = requireActivity().findViewById(R.id.main_page_link);
        jariya.setMovementMethod(LinkMovementMethod.getInstance());

        new RecyclerSort().init(view,5);
    }

}