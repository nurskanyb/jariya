package com.projects.jaryia.service;

import android.graphics.Bitmap;

import com.cloudinary.Cloudinary;
import com.cloudinary.Transformation;
import com.cloudinary.transformation.TextLayer;
import com.cloudinary.utils.ObjectUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Callable;

public class CloudinarySave implements Callable<String> {
    private Map uploadResult;
    private String urlsPhoto = "";
    private ArrayList<Bitmap> imageLinks;

    public CloudinarySave(ArrayList<Bitmap> imageLinks) {
        this.imageLinks = imageLinks;
    }

    @Override
    public String call() {
        Cloudinary cloudinary = new Cloudinary(ObjectUtils.asMap(
                "cloud_name", "dpkxkjwmy",
                "api_key", "163832694684664",
                "api_secret", "YDYzDLzJaQIa4mghVhvX96tLXZk"));

        if (imageLinks == null)
            return ";";
        else {
            Map<String, Object> params = new HashMap<>();
            params.put("folder", "jariya");
            params.put("transformation", new Transformation().overlay(
                            "text:Arial_70_bold:Ош%20Кара-Суу!")
                    .gravity("center")
                    .color("white")
                    .crop("scale"));

            for (int i = 0; i < imageLinks.size(); i++) {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                // compress Bitmap
                imageLinks.get(i).compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] byteArray = stream.toByteArray();
                try {
                    uploadResult = cloudinary.uploader().upload(byteArray, params);
                    String url = (String) uploadResult.get("secure_url");
                    urlsPhoto = url + " " + urlsPhoto;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return urlsPhoto;
    }
}
