package com.projects.jaryia.service;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.projects.jaryia.R;
import com.projects.jaryia.model.ConfirmDate;

public class DialogPages {
    private Context context;
    private Dialog dialog;
    private TextView okay_text, cancel_text;
    private Intent newPage;
    private ConfirmDate confirmDate;

    public DialogPages(Context context, ConfirmDate confirmDate) {
        this.context = context;
        dialog = new Dialog(context);
        this.confirmDate = confirmDate;
    }

    public void show() {
        dialog.setContentView(R.layout.pay_dialog);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCancelable(false);
        dialog.getWindow().getAttributes().windowAnimations = R.style.animation;

        okay_text = dialog.findViewById(R.id.okay_text);
        cancel_text = dialog.findViewById(R.id.cancel_text);

        okay_text.setOnClickListener(v -> {
            dialog.dismiss();
            String url = "https://api.whatsapp.com/send?phone=996708298101&text=Саламатсызбы! \"" + confirmDate.getDescription()+
                    "\" ушул рекламаны приложенияга чыгарайын дедим эле";
            newPage = new Intent(Intent.ACTION_VIEW);
            newPage.setData(Uri.parse(url));
            context.startActivity(newPage);
        });

        cancel_text.setOnClickListener(v -> {
            dialog.dismiss();
            Toast.makeText(context, "Cancel clicked", Toast.LENGTH_SHORT).show();
        });

        dialog.show();
    }
}
