package com.projects.jaryia.service;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.projects.jaryia.R;
import com.projects.jaryia.RecyclerAdapter;
import com.projects.jaryia.model.News;
import com.projects.jaryia.model.PostInfo;
import com.projects.jaryia.repo.JSONPlaceHolder;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RecyclerSort {
    private ArrayList<News> allInformation = new ArrayList<>();
    private News news;

    public void init(View view, int choice) {
        clear();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://karasuukg.herokuapp.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        JSONPlaceHolder jsonPlaceHolder = retrofit.create(JSONPlaceHolder.class);
        Call<List<PostInfo>> call = jsonPlaceHolder.getPost();
        call.enqueue(new Callback<List<PostInfo>>() {
            @SuppressLint("NotifyDataSetChanged")
            @Override
            public void onResponse(@NonNull Call<List<PostInfo>> call, @NonNull Response<List<PostInfo>> response) {
                if (!response.isSuccessful()) {
                    Toast.makeText(view.getContext(), "Data is empty", Toast.LENGTH_SHORT).show();
                } else {
                    List<PostInfo> postInfos = response.body();
                    if (postInfos != null) {
                        String[] photo = new String[postInfos.size()];
                        String[] des = new String[postInfos.size()];
                        String[] data = new String[postInfos.size()];
                        String[] social = new String[postInfos.size()];
                        StringBuilder temp = new StringBuilder();
                        StringBuilder photoTemp = new StringBuilder();

                        if (choice == 0) {
                            for (int i = 0; i < postInfos.size(); i++) {
                                des[i] = postInfos.get(i).getDescription();
                                data[i] = postInfos.get(i).getAdd_date();

                                for (int j = 0; j < postInfos.get(i).getIms().size(); j++)
                                    photoTemp.append("https://res.cloudinary.com/drw2jobkz/").append(postInfos.get(i).getIms().get(j).getNumber()).append(" ");

                                for (int j = 0; j < postInfos.get(i).getNumbers().size(); j++) {
                                    temp.append(postInfos.get(i).getNumbers().get(j).getNumber()).append("\n");
                                }

                                photo[i] = photoTemp.toString();
                                photoTemp = new StringBuilder();

                                social[i] = temp.toString();
                                temp = new StringBuilder();
                            }
                            for (int i = 0; i < des.length; i++) {
                                news = new News(des[i], data[i], social[i], photo[i]);
                                allInformation.add(news);
                            }
                        } else {
                            for (int i = 0; i < postInfos.size(); i++) {
                                if (postInfos.get(i).getItemchoice() == choice) {
                                    des[i] = postInfos.get(i).getDescription();
                                    data[i] = postInfos.get(i).getAdd_date();

                                    for (int j = 0; j < postInfos.get(i).getIms().size(); j++)
                                        photoTemp.append("https://res.cloudinary.com/drw2jobkz/").append(postInfos.get(i).getIms().get(j).getNumber()).append(" ");

                                    for (int j = 0; j < postInfos.get(i).getNumbers().size(); j++) {
                                        temp.append(postInfos.get(i).getNumbers().get(j).getNumber()).append("\n");
                                    }

                                    photo[i] = photoTemp.toString();
                                    photoTemp = new StringBuilder();

                                    social[i] = temp.toString();
                                    temp = new StringBuilder();
                                }
                            }

                            for (int i = 0; i < des.length; i++) {
                                if (choice == postInfos.get(i).getItemchoice()) {
                                    news = new News(des[i], data[i], social[i], photo[i]);
                                    allInformation.add(news);
                                }
                            }
                        }

                    }
                    fromFirebaseCloudinary(view, choice);
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<PostInfo>> call, @NonNull Throwable t) {
                Toast.makeText(view.getContext(), "--------------------------" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void fromFirebaseCloudinary(View view, int choice) {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference readFirebase = database.getReference("Info Page");

        readFirebase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                ArrayList<String> authorTemp = new ArrayList<>();
                ArrayList<String> descriptionTemp = new ArrayList<>();
                ArrayList<String> imageLinksTemp = new ArrayList<>();
                ArrayList<String> numberTemp = new ArrayList<>();
                ArrayList<String> typeTemp = new ArrayList<>();
                ArrayList<String> dateTemp = new ArrayList<>();
                ArrayList<Boolean> adChecker = new ArrayList<>();
                int choices = 0;

                for (DataSnapshot userSnapshot : snapshot.getChildren()) {
                    typeTemp.add(userSnapshot.child("type").getValue(String.class));
                    if (choice == 1) {
                        if (typeTemp.get(choices).equals("К.Мулк")) {
                            authorTemp.add(userSnapshot.child("author").getValue(String.class));
                            descriptionTemp.add(userSnapshot.child("description").getValue(String.class));
                            imageLinksTemp.add(userSnapshot.child("imageLinks").getValue(String.class));
                            numberTemp.add(userSnapshot.child("number").getValue(String.class));
                            dateTemp.add(userSnapshot.child("date").getValue(String.class));
                            adChecker.add(userSnapshot.child("checker").getValue(Boolean.class));
                        }
                    } else if (choice == 2) {
                        if (typeTemp.get(choices).equals("Авто")) {
                            authorTemp.add(userSnapshot.child("author").getValue(String.class));
                            descriptionTemp.add(userSnapshot.child("description").getValue(String.class));
                            imageLinksTemp.add(userSnapshot.child("imageLinks").getValue(String.class));
                            numberTemp.add(userSnapshot.child("number").getValue(String.class));
                            dateTemp.add(userSnapshot.child("date").getValue(String.class));
                            adChecker.add(userSnapshot.child("checker").getValue(Boolean.class));
                        }
                    } else if (choice == 3) {
                        if (typeTemp.get(choices).equals("Мал чарба")) {
                            authorTemp.add(userSnapshot.child("author").getValue(String.class));
                            descriptionTemp.add(userSnapshot.child("description").getValue(String.class));
                            imageLinksTemp.add(userSnapshot.child("imageLinks").getValue(String.class));
                            numberTemp.add(userSnapshot.child("number").getValue(String.class));
                            dateTemp.add(userSnapshot.child("date").getValue(String.class));
                            adChecker.add(userSnapshot.child("checker").getValue(Boolean.class));
                        }
                    } else if (choice == 4) {
                        if (typeTemp.get(choices).equals("Алуу сатуу")) {
                            authorTemp.add(userSnapshot.child("author").getValue(String.class));
                            descriptionTemp.add(userSnapshot.child("description").getValue(String.class));
                            imageLinksTemp.add(userSnapshot.child("imageLinks").getValue(String.class));
                            numberTemp.add(userSnapshot.child("number").getValue(String.class));
                            dateTemp.add(userSnapshot.child("date").getValue(String.class));
                            adChecker.add(userSnapshot.child("checker").getValue(Boolean.class));
                        }
                    } else if (choice == 5) {
                        if (typeTemp.get(choices).equals("Жумуш")) {
                            authorTemp.add(userSnapshot.child("author").getValue(String.class));
                            descriptionTemp.add(userSnapshot.child("description").getValue(String.class));
                            imageLinksTemp.add(userSnapshot.child("imageLinks").getValue(String.class));
                            numberTemp.add(userSnapshot.child("number").getValue(String.class));
                            dateTemp.add(userSnapshot.child("date").getValue(String.class));
                            adChecker.add(userSnapshot.child("checker").getValue(Boolean.class));
                        }
                    } else if (choice == 6) {
                        if (typeTemp.get(choices).equals("Электроника")) {
                            authorTemp.add(userSnapshot.child("author").getValue(String.class));
                            descriptionTemp.add(userSnapshot.child("description").getValue(String.class));
                            imageLinksTemp.add(userSnapshot.child("imageLinks").getValue(String.class));
                            numberTemp.add(userSnapshot.child("number").getValue(String.class));
                            dateTemp.add(userSnapshot.child("date").getValue(String.class));
                            adChecker.add(userSnapshot.child("checker").getValue(Boolean.class));
                        }
                    } else if (choice == 7) {
                        if (typeTemp.get(choices).equals("Каттам")) {
                            authorTemp.add(userSnapshot.child("author").getValue(String.class));
                            descriptionTemp.add(userSnapshot.child("description").getValue(String.class));
                            imageLinksTemp.add(userSnapshot.child("imageLinks").getValue(String.class));
                            numberTemp.add(userSnapshot.child("number").getValue(String.class));
                            dateTemp.add(userSnapshot.child("date").getValue(String.class));
                            adChecker.add(userSnapshot.child("checker").getValue(Boolean.class));
                        }
                    } else {
                        authorTemp.add(userSnapshot.child("author").getValue(String.class));
                        descriptionTemp.add(userSnapshot.child("description").getValue(String.class));
                        imageLinksTemp.add(userSnapshot.child("imageLinks").getValue(String.class));
                        numberTemp.add(userSnapshot.child("number").getValue(String.class));
                        dateTemp.add(userSnapshot.child("date").getValue(String.class));
                        adChecker.add(userSnapshot.child("checker").getValue(Boolean.class));
                    }
                }
                for (int i = 0; i < authorTemp.size(); i++) {
                    if (adChecker.get(i)) {
                        news = new News(descriptionTemp.get(i), dateTemp.get(i), numberTemp.get(i), imageLinksTemp.get(i));
                        allInformation.add(news);
                    }
                }

                Collections.reverse(allInformation);

                RecyclerView recyclerView = view.findViewById(R.id.recyclerview);
                recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
                recyclerView.setHasFixedSize(true);
                recyclerView.setNestedScrollingEnabled(false);
                RecyclerAdapter recyclerAdapter = new RecyclerAdapter(view.getContext(), allInformation);
                recyclerView.setAdapter(recyclerAdapter);
                recyclerAdapter.notifyDataSetChanged();

            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Toast.makeText(view.getContext(), "Database error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void clear() {
        int size = allInformation.size();
        if (size > 0) {
            allInformation.subList(0, size).clear();
        }
    }

}
