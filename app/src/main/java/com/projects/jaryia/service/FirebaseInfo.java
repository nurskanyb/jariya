package com.projects.jaryia.service;

import android.content.Context;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.projects.jaryia.model.ConfirmDate;
import com.projects.jaryia.repo.SendConfirmAdmin;

import java.util.HashMap;

public class FirebaseInfo {
    // creating a variable for our
    // Firebase Database.
    FirebaseDatabase firebaseDatabase;

    // creating a variable for our Database
    // Reference for Firebase.
    DatabaseReference databaseReference;
    ConfirmDate confirmDate;

    public FirebaseInfo(ConfirmDate confirmDate) {
        // below line is used to get the
        // instance of our FIrebase database.
        firebaseDatabase = FirebaseDatabase.getInstance();

        // below line is used to get reference for our database.
        databaseReference = firebaseDatabase.getReference();
        this.confirmDate = confirmDate;
    }

    public void addDatatoFirebase(Context context) {
        Toast.makeText(context, confirmDate.getAuthor(), Toast.LENGTH_SHORT).show();
        HashMap<String, Object> date = new HashMap<>();
        date.put("author", confirmDate.getAuthor());
        date.put("description", confirmDate.getDescription());
        date.put("number", confirmDate.getNumber());
        date.put("type", confirmDate.getType());
        date.put("imageLinks", confirmDate.getImageLinks());
        date.put("date", confirmDate.getDate());
        date.put("checker", confirmDate.getChecker());

        databaseReference.child("Info Page")
                .child(confirmDate.getAuthor())
                .setValue(date)
                .addOnSuccessListener(unused -> {
                    new Thread(()->{
                        new SendConfirmAdmin().sendToTelegram(confirmDate.getDescription());
                    }).start();
                    new DialogPages(context, confirmDate).show();
                }).addOnFailureListener(e -> {
                    Toast.makeText(context, "Ката чыкты, кайталаныз", Toast.LENGTH_SHORT).show();
                });
    }

}
